using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;


public class MenuVictoria : MonoBehaviour
{
    public GameObject MenuVictoria1;
    public static MenuVictoria menuVictoria;

    public void Reiniciar(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    public void MenuInicial(string nombre)
    {
        SceneManager.LoadScene(nombre);
    }

    public void Salir()
    {
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void CallMenuVictoria()
    {
        MenuVictoria1.SetActive(true);
    }

}
