using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuDerrotaScript : MonoBehaviour
{
    public GameObject MenuDerrota1;

    public void Reiniciar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MenuInicial(string nombre)
    {
        SceneManager.LoadScene(0);

    }

    public void Salir()
    {
        
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void CallMenuDerrota()
    {
        MenuDerrota1.SetActive(true);
    }
}
