using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class CorazonesCode : MonoBehaviour
{
    public GameObject[] corazones;

    void Start()
    {
        corazones[0] = GameObject.Find("Corazon 1");
        corazones[1] = GameObject.Find("Corazon 2");

    }

    public void doscorazones()
    {
        corazones[0].SetActive(false);
    }
    public void uncorazon()
    {
        corazones[1].SetActive(false);
    }
}
