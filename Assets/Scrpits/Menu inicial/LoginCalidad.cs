using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LogicaCalidad : MonoBehaviour
{
    public TMP_Dropdown dropdown;
    public int calidad;

   
    void Start()
    {
       
        calidad = PlayerPrefs.GetInt("numeroDeCalidad", 3);
        dropdown.value = calidad;
        AjustarCalidad();

  
        dropdown.onValueChanged.AddListener(delegate { AjustarCalidad(); });
    }

    void Update()
    {
       
    }

    public void AjustarCalidad()
    {
 
        QualitySettings.SetQualityLevel(dropdown.value);
  
        PlayerPrefs.SetInt("numeroDeCalidad", dropdown.value);

        calidad = dropdown.value;
    }
}
