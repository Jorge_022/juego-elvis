using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogicaVolumen : MonoBehaviour
{
    public Slider slider;
    /*public Image imagenMute;*/
    private float sliderValue;

    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("volumenAudio", 0.5f);
        sliderValue = slider.value;
        AudioListener.volume = sliderValue;
        Debug.Log("Start: Volumen inicial: " + sliderValue);
       /* RevisarSiEstoyMute();*/
    }

    public void ChangeSlider(float valor)
    {
        sliderValue = valor;
        PlayerPrefs.SetFloat("volumenAudio", sliderValue);
        AudioListener.volume = sliderValue;
        Debug.Log("ChangeSlider: Volumen cambiado a: " + sliderValue);
       /* RevisarSiEstoyMute();*/
    }

   /* public void RevisarSiEstoyMute()
    {
        imagenMute.enabled = sliderValue == 0;
    }*/
}
