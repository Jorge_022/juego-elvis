using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoNaranja : MonoBehaviour
{
    private Rigidbody rb; // Reference to Rigidbody component
    private float movementSpeed = 5f; // Movement speed
    private float turnSpeed = 100f; // Turn speed
 
    void Start()
    {
        rb = GetComponent<Rigidbody>(); // Get Rigidbody component
    }
 
    void Update()
    {
        // Get horizontal and vertical input from WASD keys
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
 
        // Calculate movement vector based on input and speed
        Vector3 movementVector = new Vector3(horizontalInput, 0, verticalInput) * movementSpeed;
 
        // Apply movement to Rigidbody
        rb.MovePosition(transform.position + movementVector * Time.deltaTime);
 
        // Get turn input from mouse or gamepad
        float turnInput = Input.GetAxis("Mouse X");
 
        // Apply turn rotation to Rigidbody
        transform.Rotate(Vector3.up * turnInput * turnSpeed * Time.deltaTime);
    }
}
