using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bigmacmove : MonoBehaviour
{
    public float runSpeed = 7;
    public float rotationSpeed = 250;
    public Animator animator;
    private float x, y;
    public Rigidbody rb;
    public float jumpHeight = 3;
    public Transform groundCheck;
    public float groundDistance = 0.1f;
    public LayerMask groundMask;
    bool isGrounded;
    public int puntos = 0;
    public Text puntoss;
    public int vida = 3;
    public GameObject Corazones;
    private MenuVictoriaScript menuVictoriaScript;
    private MenuDerrotaScript menuDerrota;
    private CorazonesCode corazonesCode;
    public int jumpCount = 0; // Contador de saltos
    public int maxJumpCount = 2; // Número máximo de saltos
    public bool isJump = false;
    public bool suelo=false;
    public float jumpforce = 10.0f;

    void Start()
    {
        // Encuentra la instancia de MenuVictoriaScript en la escena
        menuVictoriaScript = FindObjectOfType<MenuVictoriaScript>();

        // Encuentra la instancia de MenuDerrotaScript en la escena
        menuDerrota = FindObjectOfType<MenuDerrotaScript>();

        corazonesCode = FindObjectOfType<CorazonesCode>();



    }

    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        transform.Rotate(0, x * Time.deltaTime * rotationSpeed, 0);
        transform.Translate(0, 0, y * Time.deltaTime * runSpeed);

        animator.SetFloat("VelX", x);
        animator.SetFloat("VelY", y);

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        isJump = Input.GetButtonDown("Jump");
 
        if (Input.GetButtonDown("Jump") && jumpCount < maxJumpCount)
        {
            jumpCount++;
            rb.AddForce(new Vector3(0, jumpforce, 0), ForceMode.Impulse);
        }


        puntoss.text = puntos.ToString();

        if (puntos == 13)
        {
            menuVictoriaScript.CallMenuVictoria();
        }

        if (vida <= 0)
        {
            menuDerrota.CallMenuDerrota();
        }else{
            UpdateHearts(vida);
        }

        if(jumpCount==2){
        StartCoroutine(ResetJumpCount1());

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Moneda")
        {
            puntos++;
            Destroy(other.gameObject, 0.1f);
        }

        if (other.tag == "Cactus")
        {
            vida--;
        }
    }
private IEnumerator ResetJumpCount1()
    {
        yield return new WaitForSeconds(2);
        jumpCount = 0;
    }
    public void UpdateHearts(int vidas)
    {
        if(vidas == 2)
        {
            corazonesCode.doscorazones();
        }
        if(vidas == 1 )
        {
            corazonesCode.uncorazon();

        }
    }

}
