using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour{

public int maxLives = 3;
public int currentLives;


    void Start()
    {
        // Inicializar las vidas actuales con el máximo de vidas
        currentLives = maxLives;
    }

    // Método para recibir daño
    public void TakeDamage()
    {
        if (currentLives > 0)
        {
            currentLives--;

            if (currentLives <= 0)
            {
                Die();
            }
        }
    }

    // Método para manejar la muerte del personaje
    void Die()
    {
        Debug.Log("El personaje ha muerto.");
        // Aquí puedes agregar cualquier lógica adicional para la muerte del personaje
        // Por ejemplo, reiniciar el nivel, mostrar una pantalla de "Game Over", etc.
    }

    // Método opcional para recuperar vida
    public void Heal(int amount)
    {
        currentLives = Mathf.Min(currentLives + amount, maxLives);
        Debug.Log("Vida recuperada. Vidas actuales: " + currentLives);
    }



public class EnemyAttack : MonoBehaviour
{
    // Referencia al script PlayerHealth en el jugador
    public PlayerHealth playerHealth;

    void OnCollisionEnter(Collision collision)
    {
        // Comprueba si el objeto con el que colisionó es el jugador
        if (collision.gameObject.CompareTag("Player"))
        {
            // Llama al método TakeDamage en el script PlayerHealth
            playerHealth.TakeDamage();
        }
    }
}
}