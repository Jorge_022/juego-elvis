using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuVictoriaScript : MonoBehaviour
{
    public GameObject MenuVictoria;

    public void MenuInicial(string nombre)
    {
        SceneManager.LoadScene(0);
    }

    public void Salir()
    {
        
        UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    public void CallMenuVictoria()
    {
        MenuVictoria.SetActive(true);
    }
}
